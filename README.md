# Bash Script, Variables, Arguments and conditions

This will be a small demo/ code along for use to learn how to use variables, arguments and conditions

By the end we'll have a small script that uses these to install nginx on an ubuntu machine when given an IP as an argument.

We'll talk about the difference between arguments and variables.

We will also look into using conditions to help us run our script - using control flow and handling errors.

## To cover

- Arguments
- Variables 