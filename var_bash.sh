## Bash script for testing arguments and user input

# Argument
# data that a function can take in and use internally
# touch <argument>
# or
# $ mv <argument1> <argument2>

# In script if you want to use arguments they are define with $#, where # is a number.
# for example

echo $1
echo $1
echo $1

# Interpolation of variable into a string

echo "this is argument 1: $1"
echo "this is argument 1: $2"
echo "this is argument 1: $3"
echo "thos is argument 1: $4" "will you break?"